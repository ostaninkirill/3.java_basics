package com.company;

// String-3 > sumDigits
// https://codingbat.com/prob/p197890


public class String3SumDigits {
    public int sumDigits(String str) {
        String newStr = str.replaceAll("\\D+", "");
        int sum = 0;
        if (newStr.length() == 0) {
            return 0;
        }
        for (int i = 0; i < newStr.length(); i++) {
            sum += Integer.parseInt(newStr.substring(i,i+1));
        }
        return sum;
    }
    public void execute() {
        System.out.println(sumDigits("aa1bc2d3"));
        System.out.println(sumDigits("aa11b33"));
        System.out.println(sumDigits("Chocolate"));
    }
}
