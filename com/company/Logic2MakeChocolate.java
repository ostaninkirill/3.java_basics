package com.company;

// Logic-2 > makeChocolate
// https://codingbat.com/prob/p191363

public class Logic2MakeChocolate {
    public int makeChocolate(int small, int big, int goal) {
        int a;

        if (goal >= big * 5)
            a = goal - big * 5;

        else
            a = goal % 5;

        if (a <= small)
            return a;

        return -1;
    }

    public void execute() {
        System.out.println(makeChocolate(4, 1, 9));
        System.out.println(makeChocolate(4, 1, 10));
        System.out.println(makeChocolate(4, 1, 7));
    }
}
