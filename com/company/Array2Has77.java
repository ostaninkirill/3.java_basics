package com.company;

// Array-2 > has77
// https://codingbat.com/prob/p168357


public class Array2Has77 {
    public boolean has77(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (i < nums.length - 1) {
                if (nums[i] == 7 && nums[i + 1] == 7) {
                    return true;
                }
            }
            if (i < nums.length - 2) {
                if (nums[i] == 7 && nums[i + 2] == 7) {
                    return true;
                }
            }
        }
        return false;
    }

    public void execute() {
        System.out.println(has77(new int[] {1,7,7}));
        System.out.println(has77(new int[] {1,7,1,7}));
        System.out.println(has77(new int[] {1,7,1,1,7}));
    }
}
