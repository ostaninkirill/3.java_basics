package com.company;

// String-3 > countTriple
// https://codingbat.com/prob/p195714

public class String3countTriple {
    public int countTriple(String str) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.length() > i+2) {
                if (str.charAt(i) == str.charAt(i+1) && str.charAt(i) == str.charAt(i+2))
                count++;
            }
        }
        return count;
    }

    public void execute() {
        System.out.println(countTriple("abcXXXabc"));
        System.out.println(countTriple("xxxabyyyycd"));
        System.out.println(countTriple("a"));
    }
}
