package com.company;

// Array-3 > linearIn
// https://codingbat.com/prob/p134022

public class Array3LinearIn {
    public boolean linearIn(int[] outer, int[] inner) {
        int count = 0;
        int a = 0;
        if (inner.length == 0) {
            return true;
        }


        for (int i = 0; i < outer.length; i++) {
            if (inner[a] == outer[i]) {
                count++;
                a++;
            }
            else if (outer[i] > inner[a]) {
                return false;
            }

            if(count == inner.length)
                return true;
        }
        return false;
    }

    public void execute() {
        System.out.println(linearIn(new int[] {1,2,4,6} , new int[] {2,4}));
        System.out.println(linearIn(new int[] {1,2,4,6} , new int[] {2,3,4}));
        System.out.println(linearIn(new int[] {1,2,4,4,6} , new int[] {2,4}));
    }
}
