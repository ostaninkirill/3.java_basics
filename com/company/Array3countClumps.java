package com.company;

// Array-3 > countClumps
// https://codingbat.com/prob/p193817


public class Array3countClumps {
    public int countClumps(int[] nums) {
       int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i < nums.length - 1 && nums[i] == nums[i+1]) {
                if (i == 0) {
                    count++;
                }
                else {
                    if (nums[i] != nums[i-1]){
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public void execute() {
        System.out.println(countClumps(new int[] {1,2,2,3,4,4}));
        System.out.println(countClumps(new int[] {1,1,2,1,1}));
        System.out.println(countClumps(new int[] {1,1,1,1,1}));
    }
}
