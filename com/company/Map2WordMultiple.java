package com.company;

// Map-2 > wordMultiple
// https://codingbat.com/prob/p190862


import java.util.HashMap;
import java.util.Map;

public class Map2WordMultiple {
    public Map<String, Boolean> wordMultiple(String[] strings) {
        Map<String, Boolean> map = new HashMap<>();
        for (String s : strings) {
            if (map.containsKey(s)) {
                map.put(s, true);
            }
            else {
                map.put(s, false);
            }
        }
        return map;
    }


    public void execute(){
        System.out.println(wordMultiple(new String[] {"a", "b", "a", "c", "b"}));
        System.out.println(wordMultiple(new String[] {"c", "b", "a"}));
        System.out.println(wordMultiple(new String[] {"c", "c", "c", "c"}));
    }
}
