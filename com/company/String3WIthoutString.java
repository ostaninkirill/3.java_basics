package com.company;

// String-3 > withoutString
// https://codingbat.com/prob/p192570

public class String3WIthoutString {
    public String withoutString(String base, String remove) {
        String newBase = base.replaceAll("(?i)"+remove,"");
        return newBase;
    }
    public void execute() {
        System.out.println(withoutString("Hello there", "llo"));
        System.out.println(withoutString("Hello there", "e"));
        System.out.println(withoutString("Hello there", "x"));
    }
}
