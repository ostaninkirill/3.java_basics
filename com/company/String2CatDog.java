package com.company;

// String - 2 > catDog
// https://codingbat.com/prob/p111624

public class String2CatDog {
    public boolean catDog(String str) {
        int catcount = 0;
        int dogcount = 0;
        for (int i = 0; i < (str.length() - 2) ; i++) {
            if (str.substring(i, i+3).equals("cat")){
                catcount++;
            }

            if (str.substring(i, i+3).equals("dog")){
                dogcount++;
            }
        }
        if (catcount == dogcount) {
            return true;
        }
        else {
            return false;
        }
    }
    public void execute() {
        System.out.println(catDog("catdog"));
        System.out.println(catDog("catcat"));
        System.out.println(catDog("1cat1cadodog"));
    }
}
