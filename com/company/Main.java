package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = in.nextInt();
        switch(i) {
            case (1): // Вызов первой задачи Array - 2 > has77
                Array2Has77 has77 = new Array2Has77();
                has77.execute();
                break;

            case (2): // Вызов второй задачи Array-3 > linearIn
                Array3LinearIn linearIn = new Array3LinearIn();
                linearIn.execute();
                break;

            case (3): // Вызов третьей задачи Array-3 > countClumps
                Array3countClumps countClumps = new Array3countClumps();
                countClumps.execute();
                break;

            case (4): // Вызов четвертой задачи String - 2 > catDog
                String2CatDog string2 = new String2CatDog();
                string2.execute();
                break;

            case (5): // Вызов пятой задачи String-3 > withoutString
                String3WIthoutString string3 = new String3WIthoutString();
                string3.execute();
                break;

            case (6): // Вызов шестой задачи Map-2 > wordMultiple
                Map2WordMultiple multiple = new Map2WordMultiple();
                multiple.execute();
                break;

            case (7): // Вызов седьмой задачи Logic-2 > makeChocolate
                Logic2MakeChocolate logic2 = new Logic2MakeChocolate();
                logic2.execute();
                break;

            case (8): // Вызов восьмой задачи String-3 > sumDigits
                String3SumDigits SumDigits = new String3SumDigits();
                SumDigits.execute();
                break;

            case(9): // Вызов девятой задачи String-3 > countTriple
                String3countTriple countTriple = new String3countTriple();
                countTriple.execute();
                break;

        }
    }
}
